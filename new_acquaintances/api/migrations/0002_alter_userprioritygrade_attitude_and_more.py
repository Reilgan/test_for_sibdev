# Generated by Django 4.0.7 on 2022-09-11 11:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprioritygrade',
            name='attitude',
            field=models.CharField(choices=[('positive', 'positive'), ('negative', 'negative')], max_length=8, null=True),
        ),
        migrations.AlterField(
            model_name='userprioritygrade',
            name='importance',
            field=models.PositiveSmallIntegerField(null=True),
        ),
    ]

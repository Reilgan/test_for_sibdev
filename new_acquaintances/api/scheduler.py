from apscheduler.schedulers.background import BackgroundScheduler
from django.conf import settings
from django.core.mail import send_mail
from .models import User, Priority


def send_email_popular_preferences():
    users = User.objects.filter(is_confirmed=True)
    properties = Priority.objects.all()
    popular_priorities = []
    for pr in properties:
        count = pr.users.count()
        if count:
            popular_priorities.append((pr.title, count))

    popular_properties = sorted(popular_priorities, key=lambda item: item[1])
    popular_priorities = popular_properties[-3:]

    for user in users:
        if settings.DEBUG is False:
            send_mail(
                'Subject here',
                f'popular_preferences: {popular_priorities}',
                settings.EMAIL_HOST_USER,
                [user.email],
                fail_silently=False,
            )
        else:
            print(f'Popular_preferences: {popular_priorities} sent to email {user.email}')


def start():
    scheduler = BackgroundScheduler()
    scheduler.add_job(send_email_popular_preferences, 'cron', hour=settings.SEND_EMAIL_POPULAR_PRIORITY_TIME)
    scheduler.start()
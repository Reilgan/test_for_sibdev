from django.urls import path

from .views import (LoginAPIView, RegistrationAPIView, ConfirmedEmailAPIView, PrioritySetApiView,
                    PriorityReflectorRatingApiView)

app_name = 'api'
urlpatterns = [
    path('users/', RegistrationAPIView.as_view()),
    path('users/confirm_email', ConfirmedEmailAPIView.as_view()),
    path('users/login', LoginAPIView.as_view()),
    path('priority/create_or_update', PrioritySetApiView.as_view()),
    path('priority/get', PrioritySetApiView.as_view()),
    path('priority/delete', PrioritySetApiView.as_view()),
    path('priority/reflector_rating', PriorityReflectorRatingApiView.as_view()),
]

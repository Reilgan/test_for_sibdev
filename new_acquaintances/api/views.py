from datetime import datetime, timezone

from jwt import ExpiredSignatureError
from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import (LoginSerializer, RegistrationSerializer, ConfirmedEmailSerializer,
                          PriorityCreateOrUpdateSerializer, PriorityDeleteSerializer)
from .models import User, Priority, UserPriorityGrade
from .untils import DecodeConfirmEmailToken

from django.conf import settings
from django.core.mail import send_mail


class RegistrationAPIView(APIView):
    """
    """
    permission_classes = (AllowAny,)
    serializer_class = RegistrationSerializer

    def post(self, request):
        user = {
            'email': request.data.get('email'),
            'username': request.data.get('username'),
            'first_name': request.data.get('first_name'),
            'last_name': request.data.get('last_name'),
            'password': request.data.get('password')
        }

        serializer = self.serializer_class(data=user)
        serializer.is_valid(raise_exception=True)

        user = User(**user)
        confirm_email_token, confirm_email_code = user.confirm_email_token

        if settings.DEBUG is False:
            send_mail(
                'Subject here',
                f'confirm code: {confirm_email_code}',
                settings.EMAIL_HOST_USER,
                [user.email],
                fail_silently=False,
            )
        else:
            print(f'Confirm code: {confirm_email_code} sent to email {user.email}')

        return Response({'confirm_email_token': confirm_email_token}, status=status.HTTP_201_CREATED)


class ConfirmedEmailAPIView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = ConfirmedEmailSerializer

    def post(self, request):
        confirm_code = request.data.get('confirm_code', None)
        token = request.data.get('token', None)

        serializer = self.serializer_class(data={'confirm_code': confirm_code,
                                                 'token': token})
        serializer.is_valid(raise_exception=True)

        try:
            decode_data = DecodeConfirmEmailToken(token=token)
            if int(confirm_code) != int(decode_data.confirm_code):
                return Response({'error': 'Incorrect code'}, status=status.HTTP_200_OK)
        except ExpiredSignatureError:
            return Response({'error': 'Signature has expired'}, status=status.HTTP_200_OK)

        if confirm_code != decode_data.confirm_code:
            Response({'error': 'Incorrect code'}, status=status.HTTP_200_OK)

        user = User.objects.create_user(username=decode_data.username,
                                        email=decode_data.email,
                                        password=decode_data.password,
                                        last_name=decode_data.last_name,
                                        first_name=decode_data.first_name,
                                        is_confirmed=True)

        return Response({'token': user.token}, status=status.HTTP_201_CREATED)


class LoginAPIView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = LoginSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response({'token': serializer.data.get('token')}, status=status.HTTP_200_OK)


class PrioritySetApiView(APIView):
    permission_classes = (IsAuthenticated, )

    def post(self, request):
        serializer = PriorityCreateOrUpdateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        priority, created = Priority.create_or_update(user=request.user, **serializer.data)
        if created:
            return Response({'priority': priority.pk}, status=status.HTTP_201_CREATED)
        else:
            return Response({'priority': priority.pk}, status=status.HTTP_200_OK)

    def get(self, request):
        response = {}
        priorities = Priority.objects.filter(users__pk=request.user.pk)

        for priority in priorities:
            user_priority_grades = UserPriorityGrade.objects.get(user=request.user, priority=priority)
            response[priority.title] = {'attitude': user_priority_grades.attitude,
                                        'importance': user_priority_grades.importance}

        return Response(response, status=status.HTTP_200_OK)

    def delete(self, request):
        serializer = PriorityDeleteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        try:
            priority = Priority.objects.get(title=request.data.get('title'))
            priority.users.remove(request.user)

            return Response({}, status=status.HTTP_200_OK)
        except Priority.DoesNotExist:
            return Response({'Error': 'Priority not found'}, status=status.HTTP_200_OK)


class PriorityReflectorRatingApiView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        response = Priority.get_priority_reflector_rating(request.user)
        return Response(response, status=status.HTTP_200_OK)





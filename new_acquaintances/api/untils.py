import jwt
from django.conf import settings


class DecodeAuthToken:
    def __init__(self, token):
        decode = jwt.decode(token, settings.PUBLIC_JWT_KEYS, algorithms=["RS256"])
        self.id = decode.get('id')
        self.exp = decode.get('exp')


class DecodeConfirmEmailToken:
    def __init__(self, token):
        decode = jwt.decode(token, settings.PUBLIC_JWT_KEYS, algorithms=["RS256"])
        self.exp = decode.get('exp')
        self.username = decode.get('username')
        self.email = decode.get('email')
        self.first_name = decode.get('first_name')
        self.last_name = decode.get('last_name')
        self.password = decode.get('password')
        self.confirm_code = decode.get('confirm_code')
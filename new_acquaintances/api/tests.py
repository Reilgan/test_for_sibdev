import random
import time

from rest_framework.test import APITestCase, APIClient
from django.test.utils import override_settings
import json
import transliterate

from .models import Priority, User, UserPriorityGrade
from .untils import DecodeConfirmEmailToken


class WorkFromTestData(APITestCase):
    users = list()
    client = APIClient()

    @classmethod
    @override_settings(DEBUG=True)
    def setUpClass(cls) -> None:
        super(WorkFromTestData, cls).setUpClass()
        with open('api/participants.json', 'r') as json_file:
            for line in json_file.readlines()[:300]:
                data = json.loads(line)
                name = data['name']
                f_name, l_name = name.split(' ')
                tr_name = transliterate.translit(name, reversed=True).replace(' ', '').replace("'", "")
                response_users = cls.client.post('/api/users/', data={'email': f'{tr_name}@mail.com',
                                                                  'username': tr_name,
                                                                  'first_name': f_name,
                                                                  'last_name': l_name,
                                                                  'password': '3316F2213'})

                confirm_email_token = response_users.data.get('confirm_email_token')
                try:
                    decode_confirm_code = DecodeConfirmEmailToken(token=confirm_email_token).confirm_code
                except:
                    print(f'    Error Create user, username: {tr_name}')
                    continue

                response_auth_token = cls.client.post('/api/users/confirm_email', data={'token': confirm_email_token,
                                                                                        'confirm_code': decode_confirm_code})

                print(f'    Create user, username: {tr_name}, response: {response_auth_token}')

                cls.users.append({
                    'email': f'{tr_name}@mail.com',
                    'username': tr_name,
                    'first_name': f_name,
                    'last_name': l_name,
                    'password': '3316F2213',
                    'auth_token': response_auth_token.data.get("token"),
                    'precedents': data['precedents'],
                })
        cls.api_login()
        cls.api_priority_create()

    @classmethod
    @override_settings(DEBUG=True)
    def tearDownClass(cls):
        cls.api_priority_delete()
        super().tearDownClass()

    @classmethod
    def api_login(cls):
        for user in cls.users:
            response = cls.client.post('/api/users/login', data={'username': user['username'],
                                                                  'password': user['password']})

            print(f'    Login user, username: {user["username"]}, response: {response}')

    @classmethod
    def api_priority_create(cls):
        for user in cls.users:
            cls.client.credentials(HTTP_AUTHORIZATION='Token ' + user['auth_token'])
            for k, v in user['precedents'].items():
                response = cls.client.post('/api/priority/create_or_update', data={'title': k,
                                                                             'attitude': v['attitude'],
                                                                             'importance': v['importance']})

                priority = Priority.objects.get(title=k)
                user_obj = User.objects.get(username=user['username'])

                cls.assertTrue(cls, user_obj is not None, msg='')
                cls.assertTrue(cls, priority is not None, msg='')
                cls.assertTrue(cls, user_obj in priority.users.all(), msg='')

                user_priority_grade = UserPriorityGrade.objects.get(user=user_obj, priority=priority)

                cls.assertTrue(cls, user_priority_grade is not None, msg='')

                cls.assertTrue(cls,
                               all([user_priority_grade.attitude == v['attitude'],
                                    user_priority_grade.importance == v['importance']]),
                               msg='')

                print(f'    Create priority: {k} for user: {user["username"]}, response: {response}')

    def test_api_priority_update(self):
        for user in self.users:
            self.client.credentials(HTTP_AUTHORIZATION='Token ' + user['auth_token'])
            for k, v in user['precedents'].items():
                v['attitude'] = UserPriorityGrade.NEGATIVE if v['attitude'] == UserPriorityGrade.POSITIVE else \
                    UserPriorityGrade.POSITIVE

                v['importance'] = random.randint(1, 10)

                response = self.client.post('/api/priority/create_or_update', data={'title': k,
                                                                                    'attitude': v['attitude'],
                                                                                    'importance': v['importance']})

                priority = Priority.objects.get(title=k)
                user_obj = User.objects.get(username=user['username'])

                self.assertTrue(user_obj is not None, msg='')
                self.assertTrue(priority is not None, msg='')
                self.assertTrue(user_obj in priority.users.all(), msg='')

                user_priority_grade = UserPriorityGrade.objects.get(user=user_obj, priority=priority)

                self.assertTrue(user_priority_grade is not None, msg='')

                self.assertTrue(all([user_priority_grade.attitude == v['attitude'],
                                     user_priority_grade.importance == v['importance']]),
                                msg='')

                print(f'    Update priorityGrade: {user_priority_grade.pk} for user: '
                      f'{user["username"]}, response: {response}')

    def test_api_priority_get(self):
        for user in self.users:
            self.client.credentials(HTTP_AUTHORIZATION='Token ' + user['auth_token'])
            response = self.client.get('/api/priority/get')
            print(f'    Get priorities for user: {user["username"]}, response: {response}')

    def test_api_reflector_rating(self):
        for user in self.users:
            self.client.credentials(HTTP_AUTHORIZATION='Token ' + user['auth_token'])
            response = self.client.get('/api/priority/reflector_rating')
            print(f'    Get reflector_rating for user: {user["username"]}, response: {response.data}')


    @classmethod
    def api_priority_delete(cls):
        for user in cls.users:
            cls.client.credentials(HTTP_AUTHORIZATION='Token ' + user['auth_token'])

            for k in user['precedents'].keys():
                response = cls.client.delete(f'/api/priority/delete', data={'title': k})

                priority = Priority.objects.get(title=k)
                user_obj = User.objects.get(username=user['username'])

                cls.assertTrue(cls, user_obj is not None, msg='')
                cls.assertTrue(cls, priority is not None, msg='')
                cls.assertFalse(cls, user_obj in priority.users.all(), msg='')

                print(f'    Delete priorities for user: {user["username"]}, response: {response.data}')

import datetime

from rest_framework import serializers
from django.contrib.auth import authenticate

from .models import User, UserPriorityGrade


class RegistrationSerializer(serializers.ModelSerializer):
    """ Сериализация регистрации пользователя и создания нового. """

    password = serializers.CharField(
        max_length=128,
        min_length=8,
        write_only=True
    )

    class Meta:
        model = User
        fields = ['email', 'username', 'first_name', 'last_name', 'password']


class ConfirmedEmailSerializer(serializers.Serializer):
    confirm_code = serializers.IntegerField(style={'base_template': 'input.html'})
    token = serializers.CharField(style={'base_template': 'textarea.html'})

    def validate(self, data):
        confirm_code = data.get('confirm_code', None)
        token = data.get('token', None)

        if confirm_code is None:
            raise serializers.ValidationError(
                'An code address is required to log in.'
            )

        if token is None:
            raise serializers.ValidationError(
                'An token address is required to log in.'
            )

        return {'code': confirm_code,
                'token': token}


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=255)
    password = serializers.CharField(max_length=128)

    token = serializers.CharField(max_length=255, read_only=True)

    def validate(self, data):
        username = data.get('username', None)
        password = data.get('password', None)

        if username is None:
            raise serializers.ValidationError(
                'An username address is required to log in.'
            )

        if password is None:
            raise serializers.ValidationError(
                'A password is required to log in.'
            )

        user = authenticate(username=username, password=password)

        if user is None:
            raise serializers.ValidationError(
                'A user with this email and password was not found.'
            )

        if not user.is_active:
            raise serializers.ValidationError(
                'This user has been deactivated.'
            )

        user.last_login = datetime.datetime.now(datetime.timezone.utc)
        user.save()

        return {
            'username': username,
            'password': password,
            'token': user.token,
        }


class PriorityCreateOrUpdateSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=255)
    attitude = serializers.ChoiceField(choices=UserPriorityGrade.CHOICES)
    importance = serializers.IntegerField()


class PriorityDeleteSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=255)
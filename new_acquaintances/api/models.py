from typing import Tuple

import jwt
import random
from datetime import datetime, timedelta

from django.conf import settings
from django.contrib.auth.models import (AbstractBaseUser, BaseUserManager, PermissionsMixin)

from django.db import models, transaction


class UserManager(BaseUserManager):
    def create_user(self, username, email, first_name, last_name, password=None, is_confirmed=False):
        """ Создает и возвращает пользователя с имэйлом, паролем и именем. """
        if username is None:
            raise TypeError('Users must have a username.')

        if email is None:
            raise TypeError('Users must have an email address.')

        user = self.model(username=username,
                          email=self.normalize_email(email),
                          first_name=first_name,
                          last_name=last_name,
                          is_confirmed=is_confirmed)

        user.set_password(password)
        user.save()

        return user

    def create_superuser(self, username, email, password):
        """ Создает и возввращет пользователя с привилегиями суперадмина. """
        if password is None:
            raise TypeError('Superusers must have a password.')

        user = self.create_user(username, email, password=password, first_name=None, last_name=None)
        user.is_superuser = True
        user.is_staff = True
        user.save()

        return user


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(db_index=True, max_length=255, unique=True)
    first_name = models.CharField(max_length=255, null=True)
    last_name = models.CharField(max_length=255, null=True)
    email = models.EmailField(db_index=True, unique=True)
    is_active = models.BooleanField(default=True)
    is_confirmed = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']
    objects = UserManager()

    def __str__(self):
        return self.email

    @property
    def token(self):
        return self._generate_jwt_token()

    @property
    def confirm_email_token(self):
        return self._generate_jwt_confirm_email_token()

    def get_full_name(self):
        return self.username

    def get_short_name(self):
        return self.username

    def _generate_jwt_confirm_email_token(self):
        dt = datetime.now() + timedelta(seconds=300)
        confirm_code = random.randint(1000, 9999)
        token = jwt.encode({
            'exp': int(dt.strftime('%s')),
            'username': self.username,
            'email': self.email,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'password': self.password,
            'confirm_code': confirm_code
        }, settings.PRIVATE_JWT_KEYS, algorithm='RS256')

        return token, confirm_code

    def _generate_jwt_token(self):
        dt = datetime.now() + timedelta(days=60)

        token = jwt.encode({
            'id': self.pk,
            'exp': int(dt.strftime('%s'))
        }, settings.PRIVATE_JWT_KEYS, algorithm='RS256')

        return token


class Priority(models.Model):
    title = models.CharField(max_length=255, unique=True)
    users = models.ManyToManyField(User)

    @classmethod
    def create_or_update(cls, user, title, attitude, importance):
        with transaction.atomic():
            priority, is_priority_create = cls.objects.get_or_create(title=title)
            priority.users.add(user)
            priority.save()

            user_priority_grade, created = UserPriorityGrade.objects.get_or_create(user=user,
                                                                                   priority=priority)

            user_priority_grade.attitude = attitude
            user_priority_grade.importance = importance
            user_priority_grade.save()
            return priority, is_priority_create

    def get_user_priority_grade(self, user):
        user_priority_grade = UserPriorityGrade.objects.get(user=user, priority=self)
        return user_priority_grade

    @classmethod
    def get_priority_reflector_rating(cls, user):
        def confusion(priority_grade_):
            """
            Переводит оценку пользователя к положительному или отрицательному значению в зависимости от отношения,
            смещает оценку в положительную часть числового ряда
            возвращает отношение оценки к максимальному значению с учетом смещения в положительную часть числового ряда
            """
            absolute_value = (1 * priority_grade_.importance if
                              priority_grade_.attitude == UserPriorityGrade.POSITIVE
                              else -1 * priority_grade_.importance)

            confusion_priority = absolute_value + 11
            return confusion_priority / 21

        # Получение списка приоритетов user
        priorities = Priority.objects.filter(users__pk=user.pk)

        pre_data = []
        for priority in priorities:
            pre_data_priority = dict()
            pre_data_priority['title'] = priority.title

            # получение оценки приоритета от user
            self_priority_grade = priority.get_user_priority_grade(user)
            self_priority_grade = confusion(self_priority_grade)

            # получение пользователей с такими же приоритетами
            with_out_self_priority_grades = list(priority.users.all())
            # исключение запросившего пользователя из списка
            with_out_self_priority_grades.remove(user)

            pre_data_other_user = []
            for user in with_out_self_priority_grades:
                # получение оценки приоритета от других пользователей
                priority_grade = priority.get_user_priority_grade(user)
                priority_grade = confusion(priority_grade)

                # сравнение оценки пользователя с оценками других пользователей
                assessment_ratio = self_priority_grade / priority_grade if abs(self_priority_grade) <= abs(
                    priority_grade) \
                    else priority_grade / self_priority_grade

                pre_data_other_user.append((user.username, assessment_ratio))

            pre_data_priority['other_priority_grade'] = pre_data_other_user
            pre_data.append(pre_data_priority)

        pr_users = dict()
        for data in pre_data:
            # сумирование сравнений оценок
            for username, grade in data['other_priority_grade']:
                grade_dict = {}
                if pr_users.get(username, None) is None:
                    grade_dict['grade'] = grade
                    grade_dict['count'] = 1
                    pr_users[username] = grade_dict
                else:
                    pr_users[username]['grade'] += grade
                    pr_users[username]['count'] += 1

        response = list()
        for username, grade_dict in pr_users.items():
            # поиск среднего арифметического оценок
            grade_average = grade_dict['grade'] / grade_dict['count']
            if grade_average >= 0.75:
                response.append({
                    'username': username,
                    'grade': grade_dict['grade'] / grade_dict['count']
                })
        
        return sorted(response, key=lambda item: item['grade'])[-20:]

    def __str__(self):
        return self.title


class UserPriorityGrade(models.Model):
    POSITIVE = 'positive'
    NEGATIVE = 'negative'
    CHOICES = ((POSITIVE, 'positive'),
               (NEGATIVE, 'negative'))

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    priority = models.ForeignKey(Priority, on_delete=models.CASCADE)
    attitude = models.CharField(max_length=8, choices=CHOICES, null=True)
    importance = models.PositiveSmallIntegerField(null=True)
